CXX32=i686-w64-mingw32-g++
CXX64=x86_64-w64-mingw32-g++

MKDIR=mkdir -p
RM=rm -f
CPPFLAGS=-g -c
LDFLAGS=-g
LDLIBS=

OBJ_DIR32 = obj32
OBJ_DIR64 = obj64

SRCS=test.cpp lib.cpp
OBJS32=$(SRCS:%.cpp=$(OBJ_DIR32)/%.o)
OBJS64=$(SRCS:%.cpp=$(OBJ_DIR64)/%.o)

DEST_DIR=bin

all: dirs all32 all64

all32: test32
all64: test64

test32: $(OBJS32)
	$(CXX32) $(LDFLAGS) -o $(DEST_DIR)/test32.exe $(OBJS32) $(LDLIBS) 

test64: $(OBJS64)
	$(CXX64) $(LDFLAGS) -o $(DEST_DIR)/test64.exe $(OBJS64) $(LDLIBS) 

$(OBJ_DIR32)/%.o: %.cpp
	$(CXX32) -c $(CXXFLAGS) $< -o $@

$(OBJ_DIR64)/%.o: %.cpp
	$(CXX64) -c $(CXXFLAGS) $< -o $@

depend: .depend

.depend: $(SRCS)
	rm -f ./.depend
	$(CXX32) $(CPPFLAGS) -MM $^>>./.depend;

clean:
	$(RM) $(OBJS32)
	$(RM) $(OBJS64)

dirs:
	$(MKDIR) $(OBJ_DIR32)
	$(MKDIR) $(OBJ_DIR64)
	$(MKDIR) $(DEST_DIR)

dist-clean: clean
	$(RM) *~ .depend

include .depend

.PHONY: dirs all

